#!/bin/bash -e

JOB_DIR=/var/vcap/jobs/chef_client
RUN_DIR=/var/vcap/sys/run/chef_client
LOG_DIR=/var/vcap/sys/log/chef_client
PIDFILE=$RUN_DIR/chef_client.pid

# function to test for a lock
# expects the lock file as the first argument
function f_lock_test {

lock_pid_file=$1
# error checking
if [[ ${lock_pid_file} == "" ]]
  then
    echo "no lock file argument provided"
    return 1
fi

if [ -e ${lock_pid_file} ]
  then
    existing_pid=`cat ${lock_pid_file}`
    if kill -0 ${existing_pid} > /dev/null
      then
        echo "Process already running"
        return 1
    else
      rm ${lock_pid_file}
    fi
fi
echo $$  > ${lock_pid_file}
}

# PID/lock - we only want one instance running.
f_lock_test ${PIDFILE}

mkdir -p ${LOG_DIR}

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' chef|grep "install ok installed")
echo Checking for chef: $PKG_OK
if [ "" == "$PKG_OK" ]; then
  echo "No chef. Setting up chef."
  dpkg -i ${BOSH_INSTALL_TARGET}/chef_11.16.2-1_amd64.deb
fi

while true
  do
    chef-client >> ${LOG_DIR}/chef_client.log 2>&1
    sleep 180
  done
